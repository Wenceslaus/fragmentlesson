package com.sourceit.fragmentlesson;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class FragmentFirst extends Fragment {

    public static FragmentFirst newInstance() {
        return new FragmentFirst();
    }

    private ActivityNavigation navigation;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ActivityNavigation) {
            navigation = (ActivityNavigation) context;
        } else {
            throw new IllegalArgumentException(context.getClass().getName() + " not implemented ActivityNavigation");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_first, container, false);
        root.findViewById(R.id.action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigation.showSecond();
            }
        });
        return root;
    }
}
