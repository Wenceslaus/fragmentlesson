package com.sourceit.fragmentlesson;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements ActivityNavigation{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction()
                .replace(R.id.container, FragmentFirst.newInstance())
                .commit();

//        Button button = findViewById(R.id.action);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getSupportFragmentManager()
//                        .beginTransaction()
//                        .replace(R.id.container, FragmentSecond.newInstance())
//                        .addToBackStack(null)
//                        .commit();
//            }
//        });
    }

    @Override
    public void showSecond() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, FragmentSecond.newInstance())
                .addToBackStack(null)
                .commit();
    }
}
